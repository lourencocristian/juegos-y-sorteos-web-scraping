# Juegos y Sorteos Web Scraping 🐍

Scrap [**juegosysorteos.gob.mx**](http://www.juegosysorteos.gob.mx/) Mexican government web site using Python 🐍.

## Description

The project is to use **Python web scraping** to extract information from [**juegosysorteos.gob.mx**](http://www.juegosysorteos.gob.mx/) website to learn about that functionality and break the **SSL** problem with the page.

The problem starts in sections where embed other URL using iframe without SSL certificate but under https.

## Getting started

It's very simple to use the script to execute **Python** code in **Google Colab** environment. Just open it and run: [**juegosysorteos_WebScraping_Python.ipynb**](https://colab.research.google.com/drive/1o8GMOTJX2W9Qd7juKOH5XET8WOGnnmF3?usp=sharing)

## Usage

A simple explanation of different parts in the script.

### Last update

The first part show you the **last update** date reported for the page.

### List of licensees

Display all licensees (*permisionarios*) details with their ID.

### List of establishments for one licensees

When you execute this section you need to provide the **license ID** from the past section.

### Example

Here is an example with one output just for reference: [**Juegos y sorteos 05-07-2022**](Juegos_y_sorteos_05-07-2022.ipynb)

## Support

If you need anything feels free to contact me directly by creating an issue under the project or just emailing me.

## Contributing

I am always open to collaborations, suggestions, improvements, etc. Please **request access** and create a merge request with your proposal.
